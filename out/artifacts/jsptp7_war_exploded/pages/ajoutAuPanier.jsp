<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set target="${sessionScope.panier}"
       property="article"
       value="${ens.catalogue[param.id]}"
/>

<c:redirect url="catalogue.jsp"/>