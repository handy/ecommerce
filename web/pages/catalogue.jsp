<%@taglib prefix="c"
          uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<%@ page contentType="text/html"
         pageEncoding="UTF-8"
%>

<html>
    <head>
        <title>Catalogue des produits</title>
    </head>
    <body>
        <h1>Catalogue</h1>
        <jsp:useBean id="ens"
                     scope="application"
                     class="beans.CatalogueBean"/>

        <jsp:useBean id="panier"
                     scope="session"
                     class="beans.PanierBean"/>

        <ul>
            <c:forEach items="${ens.catalogue}" var="association">
                <c:url var="produitURL" value="produit.jsp">
                    <c:param name="id" value="${association.key}"/>
                </c:url>
            <li>
                <a href="${produitURL}">${association.value.nom}</a>
            </li>
            </c:forEach>

        </ul>

        <c:if test="${!empty panier.listeProduits}">
            Votre panier contient les livres suivants :
            <table>
                <c:forEach items="${panier.listeProduits}" var="produit">
                    <tr>
                        <td>${produit.nom}</td>
                        <td>
                            <fmt:formatNumber value="${produit.prix}" type="currency"/>
                        </td>
                    </tr>
                </c:forEach>
                <tr><td colspan="2"><hr /></td></tr>
                <tr>
                    <td><b>Total :</b></td>
                    <td>
                        <fmt:formatNumber value="${panier.total}" type="currency"/>
                    </td>
                </tr>
            </table>
        </c:if>
    </body>
</html>
