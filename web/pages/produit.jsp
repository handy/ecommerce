<%@page contentType="text/html"
        pageEncoding="UTF-8"
%>
<%@taglib prefix="c"
          uri="http://java.sun.com/jsp/jstl/core"
%>

<html>
    <head>
        <title>Description d'un produit</title>
    </head>
    <body>

        <c:set var="produit"
               value="${ens.catalogue[param.id]}"
               scope="page"/>

        <h1>${produit.nom}</h1>

        ${produit.descr} <br>
        ${produit.prix}

        <c:url var="ajoutAuPanierURL" value="ajoutAuPanier.jsp" >
            <c:param name="id" value="${produit.id}" />
        </c:url>

        <a href="${ajoutAuPanierURL}">
            Ajouter ce produit au panier
        </a>
    </body>

</html>