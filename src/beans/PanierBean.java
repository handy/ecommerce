package beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class PanierBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Set<ProduitBean> listeProduits = new HashSet<>();

    public Set<ProduitBean> getListeProduits(){return listeProduits;}

    public void setArticle(ProduitBean livre){
        this.listeProduits.add(livre);
    }


    public PanierBean(){
        ProduitBean prod = new ProduitBean();
        prod.setId("1000");
        prod.setNom("JavaServer Pages");
        prod.setDescr("Learn how to develop a JSP based web application");
        prod.setPrix(32.95f);

        listeProduits.add(prod);
    }

    public float getTotal(){
        float total = 0;
        for(ProduitBean produit: listeProduits){
            float price = produit.getPrix();
            total =+ total + price;
        }
        return total;
    }

    @Override
    public String toString() {
        return "PanierBean{" +
                "listeProduits=" + listeProduits +
                '}';
    }

}
