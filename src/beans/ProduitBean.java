package beans;

import java.io.Serializable;
import java.util.Objects;

public class ProduitBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String nom;
    private String descr;
    private float prix;

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getDescr() {
        return descr;
    }

    public float getPrix() {
        return prix;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProduitBean)) return false;
        ProduitBean that = (ProduitBean) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "ProduitBean{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", descr='" + descr + '\'' +
                ", prix=" + prix +
                '}';
    }
}
